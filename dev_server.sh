#!/bin/bash -xe

source venv/bin/activate
export FLASK_ENV=development
export FLASK_APP=chillometer
flask run --host=0.0.0.0
