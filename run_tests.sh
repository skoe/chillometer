#!/bin/bash -xe

source venv/bin/activate
#pip install -e .
coverage erase
coverage run -m pytest
coverage report
coverage html
