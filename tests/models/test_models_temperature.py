import pytest
import mongoengine
import datetime
import math
from bson import ObjectId
from io import BytesIO
from chillometer.models.temperature import Temperature


def test_when_t_too_low_then_invalid(empty_db):
    c = Temperature(temperature=-20.1)
    with pytest.raises(mongoengine.ValidationError):
        c.save()


def test_when_t_too_high_then_invalid(empty_db):
    c = Temperature(temperature=50.1)
    with pytest.raises(mongoengine.ValidationError):
        c.save()


def test_when_t_ok_high_then_save_and_load(empty_db):
    c = Temperature(temperature=23.7)
    c.save()
    t = Temperature.objects().first()
    assert math.isclose(t.temperature, 23.7, abs_tol=0.01)
