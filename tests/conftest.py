import pytest
import mongoengine
from chillometer import create_app


# We share mongoengine with the flask app, both use the default connection but
# the flask app closes it at the end of the request. That's why we can't keep it
# accross tests.
@pytest.fixture
def db():
    db = mongoengine.connect("chillometer-test")
    yield db
    mongoengine.disconnect()


@pytest.fixture
def empty_db(db):
    db.drop_database("chillometer-test")
    return db


@pytest.fixture
def client():
    test_config = {"TESTING": True, "MONGODB_DB": "chillometer-test"}
    app = create_app(test_config=test_config)
    with app.test_client() as client:
        yield client
