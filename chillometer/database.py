from pymongo import MongoClient
import mongoengine
from flask import g, current_app


def get_db() -> MongoClient:
    db = getattr(g, "db", None)
    if db is None:
        db_name = current_app.config["MONGODB_DB"]
        db = g.db = mongoengine.connect(db_name)
    return db


def close_db(_):
    db = getattr(g, "db", None)
    if db is not None:
        mongoengine.disconnect_all()
