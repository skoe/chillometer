from flask import Blueprint, render_template, make_response, abort, request
from werkzeug.exceptions import HTTPException
from ..database import get_db

from ..models.temperature import Temperature

bp = Blueprint("main", __name__)


@bp.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    resp = {
        "code": e.code,
        "name": e.name,
        "description": e.description,
    }
    return make_response(resp, e.code)


@bp.route("/")
def index():
    return render_template("index.html")


@bp.route("/manual")
def manual():
    return render_template("index.html")


@bp.route("/privacy")
def privacy():
    return render_template("index.html")


@bp.route("/legal")
def legal():
    return render_template("index.html")


@bp.route("/<int:id>/thermometer.svg")
def thermometer(id):
    if id != 0:
        abort(404, description="Temperature ID not found.")
    get_db()
    entry = Temperature.objects().first()
    if entry is None:
        t = -20.0
    else:
        t = entry.temperature
    t = max(Temperature.T_MIN, min(Temperature.T_MAX, t))
    resp = make_response(render_template("thermometer.svg", celsius=t))
    resp.headers["Content-type"] = "image/svg+xml; charset=utf-8"
    return resp


@bp.route("/api/v1.0/temperature/<int:id>", methods=["PUT"])
def update_temperature(id):
    if id != 0:
        abort(404, description="Temperature ID not found.")
    if not request.json:
        abort(400, description="Request must contain JSON.")
    if not "temperature" in request.json:
        abort(400, description="Missing JSON field 'temperature'.")
    json = request.json
    try:
        t = float(json["temperature"])
    except ValueError:
        abort(
            400,
            description="Failed to parse JSON field 'temperature', should be a float number.",
        )
    get_db()
    entry = Temperature.objects().first()
    if entry is None:
        entry = Temperature(temperature=t)
    else:
        entry.temperature = t
    entry.save()
    return {"temperature": t}
