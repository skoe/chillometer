from mongoengine import Document, FloatField


class Temperature(Document):
    T_MIN = -20.0
    T_MAX = 50.0
    temperature = FloatField(required=True, min_value=T_MIN, max_value=T_MAX)
