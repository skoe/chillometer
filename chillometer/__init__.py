from flask import Flask, Response
from chillometer.database import close_db

DEFAULTS = {
    "MONGODB_DB": "chillometer"
}

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_mapping(test_config or DEFAULTS)
    app.teardown_appcontext(close_db)

    from .views import main

    app.register_blueprint(main.bp)

    return app
