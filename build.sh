#!/bin/bash -xe

source venv/bin/activate
rm -rf *.egg-info build dist
python setup.py bdist_wheel
