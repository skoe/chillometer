#!/bin/bash

if [ "$#" != "4" ]; then
  echo "Adapt this script to your needs. Do not execute it w/o checking it first"
  echo "Read and understand the script before executing it :P"
  exit 1
fi

if [ "$UID" = "0" ]; then
  echo "Do not execute this as root, is uses sudo when needed."
  exit 1
fi

set -ex

# Domain the app is running at, e.g., example.com
DOMAIN=$1

# Domain the app is running at, e.g., /chillometer
LOCATION=$2

# Set up HTTPS via Let's Encrypt using certbot, y|n
DO_LETS_ENCRYPT=$3

# E-Mail address do be used when setting up the SSL certificate
EMAIL=$4

# The path of the web app package
PACKAGE=~/chillometer-0.0.dev1-py3-none-any.whl

################################################################################
###
### Install Python and pip
###
sudo apt install python3.8 python3.8-dev python3.8-venv python3-pip
sudo -H pip3 install --upgrade pip setuptools wheel

################################################################################
###
### Install Web App dependencies
###
sudo apt install nginx mongodb --no-install-recommends

################################################################################
###
### Update the firewall
###
sudo ufw allow 'Nginx Full'
sudo ufw status

################################################################################
###
### Stop the server and deactivate the services, if they are running
###
set +e
sudo systemctl stop chillometer
sudo systemctl stop nginx
set -e

################################################################################
###
### Set up HTTPS via Let's Encrypt using certbot
###
if [ "$DO_LETS_ENCRYPT" = "y" ]; then
  sudo apt-get install certbot python3-certbot-nginx
  sudo certbot --nginx -d $DOMAIN -d www.$DOMAIN -m $EMAIL --redirect --agree-tos -n
fi

################################################################################
###
### Install a venv, the Gunicorn WSGI HTTP Server and the Web App
###
DEST=/var/www/chillometer
sudo rm -rf $DEST
sudo mkdir -p $DEST
sudo chown $USER $DEST
pushd $DEST
python3.8 -m venv venv
source venv/bin/activate
python -m pip install gunicorn
python -m pip install $PACKAGE
deactivate
popd
sudo chown -R www-data:www-data $DEST
sudo chmod -R o-rwx $DEST

################################################################################
###
### Configure and enable nginx server block
###
sudo sh -c "cat <<EOT > /etc/nginx/sites-available/chillometer
server {
    listen 80;
    server_name $DOMAIN www.$DOMAIN;
    real_ip_header X-Forwarded-For;

    location $LOCATION {
        include proxy_params;
        proxy_pass http://unix:/var/www/chillometer/chillometer.sock;
    }
}
EOT"
sudo ln -sf /etc/nginx/sites-available/chillometer /etc/nginx/sites-enabled/chillometer

################################################################################
###
### Configure and service for Gunicorn with our app
###
sudo sh -c "cat <<EOT > /etc/systemd/system/chillometer.service
[Unit]
Description=Gunicorn instance to serve Chill-o-meter
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/var/www/chillometer/
Environment=PATH=/var/www/chillometer/venv/bin SCRIPT_NAME=$LOCATION
ExecStart=/var/www/chillometer/venv/bin/gunicorn --workers 5 \
    --bind unix:chillometer.sock -m 007 'chillometer:create_app()'

[Install]
WantedBy=multi-user.target
EOT"

################################################################################
###
### Configure and enable our app service and nginx
###
sudo systemctl daemon-reload

sudo systemctl start chillometer
sudo systemctl enable chillometer
systemctl status chillometer

sudo systemctl start nginx
sudo systemctl enable nginx
systemctl status nginx

sleep 3

################################################################################
###
### Print logs
###
echo "***** Gunicorn service logs: *****"
sudo journalctl -u chillometer -n 10

echo "***** nginx service log: *****"
sudo journalctl -u nginx -n 10

echo "***** nginx error log: *****"
sudo tail /var/log/nginx/error.log

echo "***** nginx access log: *****"
sudo tail /var/log/nginx/access.log
