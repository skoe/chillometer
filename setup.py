import glob
from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

# we use fixed versions for a reproducible install
with open("requirements.txt", "r") as fh:
    intall_requires = fh.readlines()

data_files = glob.glob("./chillometer/static/**/*", recursive=True)
data_files += glob.glob("./chillometer/templates/**/*", recursive=True)
data_files = [f.replace("./chillometer/", "") for f in data_files]

setup(
    name="chillometer",
    version="0.0.dev1",
    author="Thomas Giesel",
    author_email="skoe@directbox.com",
    description="Chill-o-meter - A thermometer for your web app",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    package_data={ "": data_files},
    python_requires=">=3.8",
    install_requires=intall_requires
)
